<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Listing;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListingFactory extends Factory
{
    protected $model = Listing::class;

    public function definition()
    {
        $title = $this->faker->sentence(rand(5, 7));

        $content = '';
        for ($i=0; $i < 5; $i++) { 
            $content .= '<p class="mb-4">' . $this->faker->sentences(rand(5, 10), true) . '</p>';
        }

        $datetime = $this->faker->dateTimeBetween('-1 month', 'now');

        return [
            'title'          => $title,
            'slug'           => Str::slug($title). '-' . rand(1111, 9999),
            'company'        => $this->faker->company(),
            'location'       => $this->faker->country(),
            'logo'           => basename($this->faker->image(storage_path('app/public'))),
            'is_highlighted' => (rand(1, 9) > 7),
            'is_active'      => true,
            'content'        => $content,
            'apply_link'     => $this->faker->url(),
            'user_id'        => User::inRandomOrder()->first()->id,
            'created_at'     => $datetime,
            'updated_at'     => $datetime,
        ];
    }
}
